﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

	public bool youwin;
	public bool gameover;
	public bool startGame = false;	
	public AudioSource menuSong;
	public AudioSource gameplaySong;

	private PanelControl control;

	// Use this for initialization
	void Start () {
		
		control = GameObject.Find("PanelControl").GetComponent<PanelControl>();
		
		control.CloseYouwinPanel();
        control.CloseGameoverPanel();
        control.CloseGameplayPanel();
        control.OpenMenuPanel();
		menuSong.Play();
	}
	
	// Update is called once per frame
	void Update () {
	

		
	}

    public void StartGame()
    {
    	control.CloseGameoverPanel();
        control.CloseMenuPanel();
		control.CloseYouwinPanel();
        control.OpenGameplayPanel();
		menuSong.Stop();
		gameplaySong.Play();
        Time.timeScale = 1f;
    }

    public void GameOver()
    {
    	
    	Invoke("ResetGame",0.8f);
        control.OpenGameoverPanel();
    }

    public void ResetGame()
    {
    	Time.timeScale = 0f;
    	GameObject player = GameObject.Find("Player");
    	player.transform.position = new Vector3(0, 0, 0);
    	Animator anim = player.GetComponent<Animator>();
    	anim.SetBool ("Running", false);
    	anim.SetBool ("Dying", false);

    }

	public void Youwin(){

		Invoke("ResetGame",0.8f);
        control.OpenYouwinPanel();

	}

	
}

