﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelControl : MonoBehaviour {

   	public GameObject menuPanel;
    public GameObject gameoverPanel;
    public GameObject gameplayPanel;
    public GameObject youwinPanel;
	public GameObject exitGameoverButton;
	public GameObject exitMenuButton;
	public GameObject retryButton;
	public GameObject playButton;
	public GameObject retryYouwinButton;
	public GameObject exitYouwinButton;


    public void OpenGameoverPanel() { gameoverPanel.SetActive(true); }
    public void CloseGameoverPanel() { gameoverPanel.SetActive(false); }

    public void OpenGameplayPanel() { gameplayPanel.SetActive(true); }
    public void CloseGameplayPanel() { gameplayPanel.SetActive(false); }

    public void OpenMenuPanel() { menuPanel.SetActive(true); }
    public void CloseMenuPanel() { menuPanel.SetActive(false); }

	public void OpenYouwinPanel(){ youwinPanel.SetActive(true);}
	public void CloseYouwinPanel(){ youwinPanel.SetActive(false);}

    public void AplQuit(){Application.Quit();}
}