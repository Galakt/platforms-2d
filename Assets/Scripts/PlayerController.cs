﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float maxS = 11f;
	public bool dead;
	public bool grounded;
	public float jumpForce = 500f;
	public AudioSource death;
	public AudioSource iniJump;
	public AudioSource landing;


	private Rigidbody2D rb2d = null;
	private float move = 0f;
	private Animator anim;
	private bool flipped = false;
	private GameManager manager;

	void Awake () {

		rb2d = GetComponent<Rigidbody2D>();

		anim = GetComponent<Animator>();

		manager = GameObject.Find("Manager").GetComponentInParent<GameManager>();

	}

	// Update is called once per frame
	void FixedUpdate () {

		move = Input.GetAxis("Horizontal");
		rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

		if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f) {
			if ((rb2d.velocity.x < -0.001f && !flipped) || (rb2d.velocity.x > -0.001f && flipped))  {
				flipped = !flipped;
				this.transform.rotation = Quaternion.Euler(0,flipped?180:0,0);
			}
			anim.SetBool ("Running", true);

		} else {
			anim.SetBool ("Running", false);
		}

		anim.SetBool("Grounded",grounded);

		if (Input.GetButtonDown ("Jump") && grounded) {
			
			rb2d.AddForce(Vector2.up * jumpForce);
			iniJump.Play();

		}
	}

	void OnCollisionEnter2D(Collision2D coll){
		if (coll.gameObject.tag == "Death") {
			dead = true;
			anim.SetBool ("Dying", true);
			death.Play();
			manager.GameOver();
		}
		else if(coll.gameObject.tag == "Finish"){
			manager.Youwin();
		}
	}


	void Restart(){

		transform.position = Vector3.zero;
		anim.SetBool ("Dying", false);
		dead = false;
		anim.Play ("idle");
	}
}